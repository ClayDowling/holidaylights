#ifndef _MOCK_TIME_H_
#define _MOCK_TIME_H_

#include <time.h>

void time_will_return(time_t val);

#endif