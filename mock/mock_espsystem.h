#ifndef _MOCK_ESPSYSTEM_H_
#define _MOCK_ESPSYSTEM_H_

#include "esp_system.h"

void esp_random_will_return(uint32_t val);

#endif