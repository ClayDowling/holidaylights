#ifndef _MOCK_QUEUE_H_
#define _MOCK_QUEUE_H_

#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"

struct xQueueGenericCreate_call {
  UBaseType_t uxQueueLength;
  UBaseType_t uxItemSize;
  uint8_t ucQueueType;
};

struct xQueueGenericCreate_call *xQueueGenericCreateCall(void);
void xQueueGenericCreate_will_return(QueueHandle_t val);

QueueHandle_t uxQueueMessagesWaiting_Call(void);
void uxQueueMessageWaiting_will_return(UBaseType_t val);

#endif