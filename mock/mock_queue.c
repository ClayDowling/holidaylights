#include "mock_queue.h"

struct xQueueGenericCreate_call create_call = {
    .uxQueueLength = 0, .uxItemSize = 0, .ucQueueType = 0};
QueueHandle_t createWillReturn = NULL;
QueueHandle_t waitingCalledWith = NULL;
UBaseType_t waitingWillReturn = 0;

QueueHandle_t xQueueGenericCreate(const UBaseType_t uxQueueLength,
                                  const UBaseType_t uxItemSize,
                                  const uint8_t ucQueueType) {
  create_call.uxQueueLength = uxQueueLength;
  create_call.uxItemSize = uxItemSize;
  create_call.ucQueueType = ucQueueType;

  return createWillReturn;
}

struct xQueueGenericCreate_call *xQueueGenericCreateCall(void) {
  return &create_call;
}
void xQueueGenericCreate_will_return(QueueHandle_t val) {
  createWillReturn val;
}

UBaseType_t uxQueueMessagesWaiting(const QueueHandle_t xQueue) {
  waitingCalledWith = xQueue;
  return waitingWillReturn;
}

QueueHandle_t uxQueueMessagesWaiting_Call(void) { return waitingCalledWith; }

void uxQueueMessageWaiting_will_return(UBaseType_t val) {
  waitingWillReturn = val;
}