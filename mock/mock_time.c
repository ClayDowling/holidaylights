#include "mock_time.h"

#include <stdarg.h>

time_t time_value = 0;

void time_will_return(time_t val) { time_value = val; }

time_t time(time_t *tloc) {
  if (tloc) {
    *tloc = time_value;
  }
  return time_value;
}