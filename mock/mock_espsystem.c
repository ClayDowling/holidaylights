#include "mock_espsystem.h"

uint32_t esp_random_value = 0;

void esp_random_will_return(uint32_t val) { esp_random_value = val; }

uint32_t esp_random(void) { return esp_random_value; }