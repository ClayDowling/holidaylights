#ifndef _ANIMATION_H_
#define _ANIMATION_H_

#include "color.h"
#include "ws2812/ws2812_control.h"

#include <freertos/FreeRTOS.h>
#include <freertos/queue.h>
#include <freertos/task.h>

extern struct led_state lights;

#define INITIAL_STATE -1

typedef struct segment {
  unsigned int start;
  unsigned int length;
  color_t *colors;
  int state;
  time_t next_action;
} segment_t;

typedef enum animation_status {
  animation_running,
  animation_finished,
  animation_noend,
  animation_max
} a_status;

// Animations return nothing, and take a segment to act upon.
typedef a_status (*animation)(segment_t *);

extern animation current_animation;
extern QueueHandle_t animation_queue;
extern segment_t all_lights;

// Set up
void animationmanager_init(void);

// Animation Manager task
void animation_task(void *parameters);

// Implement the logic for running animations
void animation_manager(void);

// Animations
a_status fadein(segment_t *seg);
a_status fadeout(segment_t *seg);
a_status colorcycle(segment_t *seg);

#endif
