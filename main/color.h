//
// Created by clay on 11/23/2019.
//

#ifndef HOLIDAYLIGHTS_COLOR_H
#define HOLIDAYLIGHTS_COLOR_H

#include <stdint.h>
#include <stdlib.h>

typedef uint32_t color_t;

#define COLORLIST_END 0xffffffff
#define COLORLIST(...)                                                         \
  { __VA_ARGS__, COLORLIST_END }

size_t colorlist_size(color_t *);

// Material Design pallet

#define red 0xf44336
#define red50 0xffebee
#define red100 0xffcdd2
#define red200 0xef9a9a
#define red300 0xe57373
#define red400 0xef5350
#define red500 0xf44336
#define red600 0xe53935
#define red700 0xd32f2f
#define red800 0xc62828
#define red900 0xb71c1c
#define reda100 0xff8a80
#define reda200 0xff5252
#define reda400 0xff1744
#define reda700 0xd50000

/* Pink */
#define pink 0xe91e63
#define pink50 0xfce4ec
#define pink100 0xf8bbd0
#define pink200 0xf48fb1
#define pink300 0xf06292
#define pink400 0xec407a
#define pink500 0xe91e63
#define pink600 0xd81b60
#define pink700 0xc2185b
#define pink800 0xad1457
#define pink900 0x880e4f
#define pinka100 0xff80ab
#define pinka200 0xff4081
#define pinka400 0xf50057
#define pinka700 0xc51162

/* Purple */
#define purple 0x9c27b0
#define purple50 0xf3e5f5
#define purple100 0xe1bee7
#define purple200 0xce93d8
#define purple300 0xba68c8
#define purple400 0xab47bc
#define purple500 0x9c27b0
#define purple600 0x8e24aa
#define purple700 0x7b1fa2
#define purple800 0x6a1b9a
#define purple900 0x4a148c
#define purplea100 0xea80fc
#define purplea200 0xe040fb
#define purplea400 0xd500f9
#define purplea700 0xaa00ff

/* Deep Purple */
#define deeppurple 0x673ab7
#define deeppurple50 0xede7f6
#define deeppurple100 0xd1c4e9
#define deeppurple200 0xb39ddb
#define deeppurple300 0x9575cd
#define deeppurple400 0x7e57c2
#define deeppurple500 0x673ab7
#define deeppurple600 0x5e35b1
#define deeppurple700 0x512da8
#define deeppurple800 0x4527a0
#define deeppurple900 0x311b92
#define deeppurplea100 0xb388ff
#define deeppurplea200 0x7c4dff
#define deeppurplea400 0x651fff
#define deeppurplea700 0x6200ea

/* Indigo */
#define indigo 0x3f51b5
#define indigo50 0xe8eaf6
#define indigo100 0xc5cae9
#define indigo200 0x9fa8da
#define indigo300 0x7986cb
#define indigo400 0x5c6bc0
#define indigo500 0x3f51b5
#define indigo600 0x3949ab
#define indigo700 0x303f9f
#define indigo800 0x283593
#define indigo900 0x1a237e
#define indigoa100 0x8c9eff
#define indigoa200 0x536dfe
#define indigoa400 0x3d5afe
#define indigoa700 0x304ffe

/* Blue */
#define blue 0x2196f3
#define blue50 0xe3f2fd
#define blue100 0xbbdefb
#define blue200 0x90caf9
#define blue300 0x64b5f6
#define blue400 0x42a5f5
#define blue500 0x2196f3
#define blue600 0x1e88e5
#define blue700 0x1976d2
#define blue800 0x1565c0
#define blue900 0x0d47a1
#define bluea100 0x82b1ff
#define bluea200 0x448aff
#define bluea400 0x2979ff
#define bluea700 0x2962ff

/* Light Blue */
#define lightblue 0x03a9f4
#define lightblue50 0xe1f5fe
#define lightblue100 0xb3e5fc
#define lightblue200 0x81d4fa
#define lightblue300 0x4fc3f7
#define lightblue400 0x29b6f6
#define lightblue500 0x03a9f4
#define lightblue600 0x039be5
#define lightblue700 0x0288d1
#define lightblue800 0x0277bd
#define lightblue900 0x01579b
#define lightbluea100 0x80d8ff
#define lightbluea200 0x40c4ff
#define lightbluea400 0x00b0ff
#define lightbluea700 0x0091ea

/* Cyan */
#define cyan 0x00bcd4
#define cyan50 0xe0f7fa
#define cyan100 0xb2ebf2
#define cyan200 0x80deea
#define cyan300 0x4dd0e1
#define cyan400 0x26c6da
#define cyan500 0x00bcd4
#define cyan600 0x00acc1
#define cyan700 0x0097a7
#define cyan800 0x00838f
#define cyan900 0x006064
#define cyana100 0x84ffff
#define cyana200 0x18ffff
#define cyana400 0x00e5ff
#define cyana700 0x00b8d4

/* Teal */
#define teal 0x009688
#define teal50 0xe0f2f1
#define teal100 0xb2dfdb
#define teal200 0x80cbc4
#define teal300 0x4db6ac
#define teal400 0x26a69a
#define teal500 0x009688
#define teal600 0x00897b
#define teal700 0x00796b
#define teal800 0x00695c
#define teal900 0x004d40
#define teala100 0xa7ffeb
#define teala200 0x64ffda
#define teala400 0x1de9b6
#define teala700 0x00bfa5

/* Green */
#define green 0x4caf50
#define green50 0xe8f5e9
#define green100 0xc8e6c9
#define green200 0xa5d6a7
#define green300 0x81c784
#define green400 0x66bb6a
#define green500 0x4caf50
#define green600 0x43a047
#define green700 0x388e3c
#define green800 0x2e7d32
#define green900 0x1b5e20
#define greena100 0xb9f6ca
#define greena200 0x69f0ae
#define greena400 0x00e676
#define greena700 0x00c853

/* Light Green */
#define lightgreen 0x8bc34a
#define lightgreen50 0xf1f8e9
#define lightgreen100 0xdcedc8
#define lightgreen200 0xc5e1a5
#define lightgreen300 0xaed581
#define lightgreen400 0x9ccc65
#define lightgreen500 0x8bc34a
#define lightgreen600 0x7cb342
#define lightgreen700 0x689f38
#define lightgreen800 0x558b2f
#define lightgreen900 0x33691e
#define lightgreena100 0xccff90
#define lightgreena200 0xb2ff59
#define lightgreena400 0x76ff03
#define lightgreena700 0x64dd17

/* Lime */
#define lime 0xcddc39
#define lime50 0xf9fbe7
#define lime100 0xf0f4c3
#define lime200 0xe6ee9c
#define lime300 0xdce775
#define lime400 0xd4e157
#define lime500 0xcddc39
#define lime600 0xc0ca33
#define lime700 0xafb42b
#define lime800 0x9e9d24
#define lime900 0x827717
#define limea100 0xf4ff81
#define limea200 0xeeff41
#define limea400 0xc6ff00
#define limea700 0xaeea00

/* Yellow */
#define yellow 0xffeb3b
#define yellow50 0xfffde7
#define yellow100 0xfff9c4
#define yellow200 0xfff59d
#define yellow300 0xfff176
#define yellow400 0xffee58
#define yellow500 0xffeb3b
#define yellow600 0xfdd835
#define yellow700 0xfbc02d
#define yellow800 0xf9a825
#define yellow900 0xf57f17
#define yellowa100 0xffff8d
#define yellowa200 0xffff00
#define yellowa400 0xffea00
#define yellowa700 0xffd600

/* Amber */
#define amber 0xffc107
#define amber50 0xfff8e1
#define amber100 0xffecb3
#define amber200 0xffe082
#define amber300 0xffd54f
#define amber400 0xffca28
#define amber500 0xffc107
#define amber600 0xffb300
#define amber700 0xffa000
#define amber800 0xff8f00
#define amber900 0xff6f00
#define ambera100 0xffe57f
#define ambera200 0xffd740
#define ambera400 0xffc400
#define ambera700 0xffab00

/* Orange */
#define orange 0xff9800
#define orange50 0xfff3e0
#define orange100 0xffe0b2
#define orange200 0xffcc80
#define orange300 0xffb74d
#define orange400 0xffa726
#define orange500 0xff9800
#define orange600 0xfb8c00
#define orange700 0xf57c00
#define orange800 0xef6c00
#define orange900 0xe65100
#define orangea100 0xffd180
#define orangea200 0xffab40
#define orangea400 0xff9100
#define orangea700 0xff6d00

/* Deep Orange */
#define deeporange 0xff5722
#define deeporange50 0xfbe9e7
#define deeporange100 0xffccbc
#define deeporange200 0xffab91
#define deeporange300 0xff8a65
#define deeporange400 0xff7043
#define deeporange500 0xff5722
#define deeporange600 0xf4511e
#define deeporange700 0xe64a19
#define deeporange800 0xd84315
#define deeporange900 0xbf360c
#define deeporangea100 0xff9e80
#define deeporangea200 0xff6e40
#define deeporangea400 0xff3d00
#define deeporangea700 0xdd2c00

/* Brown */
#define brown 0x795548
#define brown50 0xefebe9
#define brown100 0xd7ccc8
#define brown200 0xbcaaa4
#define brown300 0xa1887f
#define brown400 0x8d6e63
#define brown500 0x795548
#define brown600 0x6d4c41
#define brown700 0x5d4037
#define brown800 0x4e342e
#define brown900 0x3e2723

/* Grey */
#define grey 0x9e9e9e
#define grey50 0xfafafa
#define grey100 0xf5f5f5
#define grey200 0xeeeeee
#define grey300 0xe0e0e0
#define grey400 0xbdbdbd
#define grey500 0x9e9e9e
#define grey600 0x757575
#define grey700 0x616161
#define grey800 0x424242
#define grey900 0x212121

/* Blue Grey */
#define bluegrey 0x607d8b
#define bluegrey50 0xeceff1
#define bluegrey100 0xcfd8dc
#define bluegrey200 0xb0bec5
#define bluegrey300 0x90a4ae
#define bluegrey400 0x78909c
#define bluegrey500 0x607d8b
#define bluegrey600 0x546e7a
#define bluegrey700 0x455a64
#define bluegrey800 0x37474f
#define bluegrey900 0x263238

/* White / Black */
#define white 0xffffff
#define black 0x000000

//
// Color Lists
//

extern color_t greys[];
extern color_t blues[];
extern color_t altblues[];

#endif // HOLIDAYLIGHTS_COLOR_H
