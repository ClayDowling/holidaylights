#include "animation.h"
#include <esp_log.h>
#include <esp_system.h>
#include <time.h>

struct led_state lights;

a_status fadein(segment_t *seg) {
  if (seg->state < 0) {
    seg->state = 0;
  }
  size_t sz = colorlist_size(seg->colors);

  // We've run out of colors, this animation is done.
  if (seg->state == sz)
    return animation_finished;

  for (int i = 0; i < NUM_LEDS; ++i) {
    lights.leds[i] = seg->colors[sz - seg->state - 1];
  }
  seg->state++;
  seg->next_action = time(NULL) + 2;

  return animation_running;
}

a_status fadeout(segment_t *seg) {
  if (seg->state < 0) {
    seg->state = 0;
  }
  size_t sz = colorlist_size(seg->colors);

  // We've run out of colors, this animation is done.
  if (seg->state == sz)
    return animation_finished;

  for (int i = 0; i < NUM_LEDS; ++i) {
    lights.leds[i] = seg->colors[seg->state];
  }
  seg->state++;
  seg->next_action = time(NULL) + 2;

  return animation_running;
}

uint32_t colorcorrect(uint32_t original) {

  // union {
  //   uint8_t component[4];
  //   uint32_t representation;
  // } color;

  // color.representation = original;

  // uint8_t r = color.component[0];
  // uint8_t g = color.component[1];
  // uint8_t b = color.component[2];

  // color.component[0] = g;
  // color.component[1] = r;
  // color.component[2] = b;

  return original;
}

a_status colorcycle(segment_t *seg) {

  if (seg->state < 0) {
    seg->state = 0;
  }
  size_t sz = colorlist_size(seg->colors);

  ESP_LOGI(__func__, "LEDs %d - %d to %x", seg->start, seg->start + seg->length,
           colorcorrect(seg->colors[seg->state]));
  for (int i = seg->start; i < seg->start + seg->length; ++i) {
    lights.leds[i] = colorcorrect(seg->colors[seg->state]);
  }
  seg->state++;
  if (seg->state >= sz)
    seg->state = 0;
  seg->next_action = time(NULL) + (esp_random() % 90) + 30;
  return animation_noend;
}
