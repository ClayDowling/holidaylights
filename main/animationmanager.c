#include "animation.h"

#include "ws2812/ws2812_control.h"
#include <esp_log.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <time.h>

animation current_animation;
time_t animation_next_action = 0;

segment_t all_lights;

void animationmanager_init(void) {
  all_lights.start = 0;
  all_lights.length = NUM_LEDS;
  xTaskCreate(animation_task, "AnimationMgr", 2048, NULL, tskIDLE_PRIORITY,
              NULL);
  ws2812_control_init();
}

void animation_task(void *parameters) {
  for (;;) {
    animation_manager();
  }
}

// Implement the logic for running animations
void animation_manager(void) {
  vTaskDelay(1000 / portTICK_PERIOD_MS);
  if (time(NULL) >= all_lights.next_action) {
    if (NULL == current_animation) {
      for (int i = all_lights.start; i < all_lights.start + all_lights.length;
           ++i) {
        lights.leds[i] = 0x0;
      }
    } else {
      current_animation(&all_lights);
      ws2812_write_leds(&lights);
      ESP_LOGI(__func__, "%ld waiting for %ld", time(NULL),
               all_lights.next_action);
    }
  }
}