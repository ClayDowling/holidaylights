//
// Created by clay on 11/23/2019.
//

#include "color.h"

color_t greys[] = COLORLIST(grey50, grey100, grey200, grey300, grey400, grey500,
                            grey600, grey700, grey800, grey900);

color_t blues[] = COLORLIST(blue50, blue100, blue200, blue300, blue400, blue500,
                            blue600, blue700, blue800, blue900, bluea100,
                            bluea200, bluea400, bluea700);

color_t altblues[] = COLORLIST(0x777777, 0x6666ff, 0x3333ff, 0x0000ff);

size_t colorlist_size(color_t *list) {
  size_t i;
  for (i = 0; list[i] != COLORLIST_END; ++i)
    ;
  return i;
}