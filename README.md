# Holiday Lights

Controller for holiday lights, using WS2812b light strings and an esp32 controller.  This code is based on the [esp-idf](https://docs.espressif.com/projects/esp-idf/en/latest/) toolkit from Espressif.

## Concepts

*ColorSet* - an array of colors to be used by a pattern

*Segment* - a series of RGB values, defined by their starting position and length, as well as a colorset.

*Animation* - a function which is applied to a segment over time.

*Pattern* - A collection of Segments, Their ColorSets, and Animations which will be applied to the segments.