#include "animation.h"
#include "mock_time.h"
#include "unity_fixture.h"

TEST_GROUP(fadeout);
TEST_SETUP(fadeout) {}
TEST_TEAR_DOWN(fadeout) {}

TEST(fadeout, fadeout_bydefault_startsWithFirstColor) {
  color_t target = greys[0];
  segment_t s = {
      .start = 0, .length = NUM_LEDS, .colors = greys, .state = INITIAL_STATE};
  fadeout(&s);

  TEST_ASSERT_EACH_EQUAL_HEX32(target, lights.leds, NUM_LEDS);
}

TEST(fadeout, fadeout_instate1_usesSecondColor) {
  color_t target = greys[1];
  segment_t s = {.start = 0, .length = NUM_LEDS, .colors = greys, .state = 1};
  fadeout(&s);
  TEST_ASSERT_EACH_EQUAL_HEX32(target, lights.leds, NUM_LEDS);
}

TEST(fadeout, fadeout_ByDefault_IncrementsStateOnEachRun) {
  segment_t s = {.start = 0, .length = NUM_LEDS, .colors = greys, .state = 1};
  fadeout(&s);
  TEST_ASSERT_EQUAL_INT(2, s.state);
}

TEST(fadeout, fadeout_whenLastColorDisplayed_ReturnsAnimationFinished) {
  segment_t s = {.start = 0, .length = NUM_LEDS, .colors = greys, .state = 1};
  s.state = colorlist_size(greys);
  TEST_ASSERT_EQUAL_INT(animation_finished, fadeout(&s));
}

TEST(fadeout, fadeout_bydefault_returnsAnimationRunning) {
  segment_t s = {.start = 0, .length = NUM_LEDS, .colors = greys, .state = 1};
  TEST_ASSERT_EQUAL_INT(animation_running, fadeout(&s));
}

TEST(fadeout, fadeout_bydefault_setsNextActionToTwoSecondsFromNow) {
  time_will_return(17);
  segment_t s = {.start = 0, .length = NUM_LEDS, .colors = greys, .state = 1};
  fadeout(&s);
  TEST_ASSERT_EQUAL_INT(19, s.next_action);
}

TEST_GROUP_RUNNER(fadeout) {
  RUN_TEST_CASE(fadeout, fadeout_bydefault_startsWithFirstColor);
  RUN_TEST_CASE(fadeout, fadeout_instate1_usesSecondColor);
  RUN_TEST_CASE(fadeout, fadeout_ByDefault_IncrementsStateOnEachRun);
  RUN_TEST_CASE(fadeout,
                fadeout_whenLastColorDisplayed_ReturnsAnimationFinished);
  RUN_TEST_CASE(fadeout, fadeout_bydefault_returnsAnimationRunning);
  RUN_TEST_CASE(fadeout, fadeout_bydefault_setsNextActionToTwoSecondsFromNow);
}
