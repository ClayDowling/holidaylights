#include "animation.h"
#include "mock_time.h"
#include "unity_fixture.h"

TEST_GROUP(fadein);
TEST_SETUP(fadein) {}
TEST_TEAR_DOWN(fadein) {}

TEST(fadein, fadein_bydefault_startsWithLastColor) {
  color_t target = greys[colorlist_size(greys) - 1];
  segment_t s = {
      .start = 0, .length = NUM_LEDS, .colors = greys, .state = INITIAL_STATE};
  fadein(&s);

  TEST_ASSERT_EACH_EQUAL_HEX32(target, lights.leds, NUM_LEDS);
}

TEST(fadein, fadein_instate1_usesSecondToLastColor) {
  color_t target = greys[colorlist_size(greys) - 2];
  segment_t s = {.start = 0, .length = NUM_LEDS, .colors = greys, .state = 1};
  fadein(&s);
  TEST_ASSERT_EACH_EQUAL_HEX32(target, lights.leds, NUM_LEDS);
}

TEST(fadein, fadein_ByDefault_IncrementsStateOnEachRun) {
  segment_t s = {.start = 0, .length = NUM_LEDS, .colors = greys, .state = 1};
  fadein(&s);
  TEST_ASSERT_EQUAL_INT(2, s.state);
}

TEST(fadein, fadein_whenLastColorDisplayed_ReturnsAnimationFinished) {
  segment_t s = {.start = 0, .length = NUM_LEDS, .colors = greys, .state = 1};
  s.state = colorlist_size(greys);
  TEST_ASSERT_EQUAL_INT(animation_finished, fadein(&s));
}

TEST(fadein, fadein_bydefault_returnsAnimationRunning) {
  segment_t s = {.start = 0, .length = NUM_LEDS, .colors = greys, .state = 1};
  TEST_ASSERT_EQUAL_INT(animation_running, fadein(&s));
}

TEST(fadein, fadein_bydefault_setsNextActionToTwoSecondsFromNow) {
  time_will_return(83);
  segment_t s = {.start = 0, .length = NUM_LEDS, .colors = greys, .state = 1};
  fadein(&s);
  TEST_ASSERT_EQUAL_INT(85, s.next_action);
}

TEST_GROUP_RUNNER(fadein) {
  RUN_TEST_CASE(fadein, fadein_bydefault_startsWithLastColor);
  RUN_TEST_CASE(fadein, fadein_instate1_usesSecondToLastColor);
  RUN_TEST_CASE(fadein, fadein_ByDefault_IncrementsStateOnEachRun);
  RUN_TEST_CASE(fadein, fadein_whenLastColorDisplayed_ReturnsAnimationFinished);
  RUN_TEST_CASE(fadein, fadein_bydefault_returnsAnimationRunning);
  RUN_TEST_CASE(fadein, fadein_bydefault_setsNextActionToTwoSecondsFromNow);
}
