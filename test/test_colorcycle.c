#include "animation.h"
#include "mock_espsystem.h"
#include "mock_time.h"
#include "unity_fixture.h"

TEST_GROUP(colorcycle);
TEST_SETUP(colorcycle) {}
TEST_TEAR_DOWN(colorcycle) {}

TEST(colorcycle, colorcycle_bydefault_startsWithFirstColor) {
  color_t target = blues[0];
  segment_t s = {
      .start = 0, .length = NUM_LEDS, .colors = blues, .state = INITIAL_STATE};
  colorcycle(&s);

  TEST_ASSERT_EACH_EQUAL_HEX32(target, lights.leds, NUM_LEDS);
}

TEST(colorcycle, colorcycle_instate1_usesSecondColor) {
  color_t target = blues[1];
  segment_t s = {.start = 0, .length = NUM_LEDS, .colors = blues, .state = 1};
  colorcycle(&s);
  TEST_ASSERT_EACH_EQUAL_HEX32(target, lights.leds, NUM_LEDS);
}

TEST(colorcycle, colorcycle_ByDefault_IncrementsStateOnEachRun) {
  segment_t s = {.start = 0, .length = NUM_LEDS, .colors = blues, .state = 1};
  colorcycle(&s);
  TEST_ASSERT_EQUAL_INT(2, s.state);
}

TEST(colorcycle, colorcycle_whenLastColorDisplayed_SetsStateToZero) {
  segment_t s = {.start = 0, .length = NUM_LEDS, .colors = blues, .state = 1};
  s.state = colorlist_size(blues) - 1;
  colorcycle(&s);
  TEST_ASSERT_EQUAL_INT(0, s.state);
}

TEST(colorcycle, colorcycle_WhenStateOffEnd_SetsStateToZero) {
  segment_t s = {.start = 0, .length = NUM_LEDS, .colors = blues, .state = 1};
  s.state = colorlist_size(blues) + 1;
  colorcycle(&s);
  TEST_ASSERT_EQUAL_INT(0, s.state);
}

TEST(colorcycle, colorcycle_bydefault_returnsAnimationNoEnd) {
  segment_t s = {.start = 0, .length = NUM_LEDS, .colors = blues, .state = 1};
  TEST_ASSERT_EQUAL_INT(animation_noend, colorcycle(&s));
}

TEST(colorcycle, colorcycle_bydefault_setsNextActionFromEspRandomAbove30) {
  segment_t s = {.start = 0, .length = NUM_LEDS, .colors = blues, .state = 1};
  time_will_return(20);
  esp_random_will_return(5);
  colorcycle(&s);
  TEST_ASSERT_EQUAL_INT(55, s.next_action);
}

TEST(colorcycle, colorcycle_bydefault_setsNextActionFromEspRandomBelow120) {
  segment_t s = {.start = 0, .length = NUM_LEDS, .colors = blues, .state = 1};
  time_will_return(20);
  esp_random_will_return(90);
  colorcycle(&s);
  TEST_ASSERT_EQUAL_INT(50, s.next_action);
}

TEST_GROUP_RUNNER(colorcycle) {
  RUN_TEST_CASE(colorcycle, colorcycle_bydefault_startsWithFirstColor);
  RUN_TEST_CASE(colorcycle, colorcycle_instate1_usesSecondColor);
  RUN_TEST_CASE(colorcycle, colorcycle_ByDefault_IncrementsStateOnEachRun);
  RUN_TEST_CASE(colorcycle, colorcycle_whenLastColorDisplayed_SetsStateToZero);
  RUN_TEST_CASE(colorcycle, colorcycle_bydefault_returnsAnimationNoEnd);
  RUN_TEST_CASE(colorcycle, colorcycle_WhenStateOffEnd_SetsStateToZero);
  RUN_TEST_CASE(colorcycle,
                colorcycle_bydefault_setsNextActionFromEspRandomAbove30);
  RUN_TEST_CASE(colorcycle,
                colorcycle_bydefault_setsNextActionFromEspRandomBelow120);
}
