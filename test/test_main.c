#include "unity_fixture.h"

static void run_all_tests(void) {
  RUN_TEST_GROUP(color);
  RUN_TEST_GROUP(fadein);
  RUN_TEST_GROUP(fadeout);
  RUN_TEST_GROUP(colorcycle);
  RUN_TEST_GROUP(animationmanager);
}

int main(int argc, const char **argv) {
  return UnityMain(argc, argv, run_all_tests);
}
