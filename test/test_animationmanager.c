#include "animation.h"
#include "mock_queue.h"
#include "mock_task.h"
#include "mock_time.h"
#include "unity_fixture.h"
#include <stdbool.h>

bool mockanimation_called;
bool otheranimation_called;
a_status mockanimation_returns;
a_status otheranimation_returns;

a_status mockanimation(segment_t *s) {
  mockanimation_called = true;
  return mockanimation_returns;
}

a_status otheranimation(segment_t *s) {
  otheranimation_called = true;
  return otheranimation_returns;
}

TEST_GROUP(animationmanager);
TEST_SETUP(animationmanager) {
  for (int i = 0; i < NUM_LEDS; ++i) {
    lights.leds[i] = 0xDEADBEEF;
  }
  all_lights.next_action = 7;
  time_will_return(7);
  current_animation = NULL;
  mockanimation_called = false;
  otheranimation_called = false;
  mockanimation_returns = animation_running;
  otheranimation_returns = animation_running;
}
TEST_TEAR_DOWN(animationmanager) {}

TEST(animationmanager, init_bydefault_startsTask) {
  animationmanager_init();
  struct xTaskCreate_call *call = xTaskCreateCall();
  TEST_ASSERT_EQUAL_PTR(animation_task, call->pvTaskCode);
}

TEST(animationmanager, am_bydefault_waitsonesecond) {
  animation_manager();
  TickType_t delay = vTaskDelayCall();
  TEST_ASSERT_EQUAL_INT(1000 / portTICK_PERIOD_MS, delay);
}

TEST(animationmanager, am_whenThereIsNoCurrentFunction_SetsAllLedsBlack) {
  current_animation = NULL;
  animation_manager();
  TEST_ASSERT_EACH_EQUAL_HEX32(0x0, lights.leds, NUM_LEDS);
}

TEST(animationmanager, am_whenThereIsCurrentAnimation_CallsAnimation) {
  current_animation = mockanimation;
  animation_manager();
  TEST_ASSERT_TRUE(mockanimation_called);
}

TEST(animationmanager, am_whenItIsNotTimeForNextAction_DoesNotCallAnimation) {
  current_animation = mockanimation;
  all_lights.next_action = 33;
  time_will_return(32);
  animation_manager();
  TEST_ASSERT_FALSE(mockanimation_called);
}

TEST(animationmanager, am_whenItIsTimeForNextAction_CallsAnimation) {
  current_animation = mockanimation;
  all_lights.next_action = 47;
  time_will_return(47);
  animation_manager();
  TEST_ASSERT_TRUE(mockanimation_called);
}

TEST_GROUP_RUNNER(animationmanager) {
  RUN_TEST_CASE(animationmanager, init_bydefault_startsTask);
  RUN_TEST_CASE(animationmanager, am_bydefault_waitsonesecond);
  RUN_TEST_CASE(animationmanager,
                am_whenThereIsNoCurrentFunction_SetsAllLedsBlack);
  RUN_TEST_CASE(animationmanager,
                am_whenThereIsCurrentAnimation_CallsAnimation);
  RUN_TEST_CASE(animationmanager,
                am_whenItIsNotTimeForNextAction_DoesNotCallAnimation);
  RUN_TEST_CASE(animationmanager, am_whenItIsTimeForNextAction_CallsAnimation);
  RUN_TEST_CASE(
      animationmanager,
      am_whenCurrentAnimationIsNoEnd_MakesAnimationInQueueNextAnimation);
}