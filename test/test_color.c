#include "unity_fixture.h"
#include "color.h"

TEST_GROUP(color);
TEST_SETUP(color) {}
TEST_TEAR_DOWN(color) {}

TEST(color, COLORLIST_byDefault_CreatesTerminatedArray)
{
    color_t actual[] = COLORLIST(0xcc00cc, 0x00cccc);
    color_t expected[] = {0xcc00cc, 0x00cccc, COLORLIST_END};

    TEST_ASSERT_EQUAL_HEX32_ARRAY(expected, actual, 3);

}

TEST(color, colorlistsize_givenlistof4_returns4)
{
    color_t actual[] = COLORLIST(0xcc00cc, 0x00cccc, 0xff0000, 0x00ff00);

    TEST_ASSERT_EQUAL_size_t(4, colorlist_size(actual));
}

TEST_GROUP_RUNNER(color) {
    RUN_TEST_CASE(color, COLORLIST_byDefault_CreatesTerminatedArray);
    RUN_TEST_CASE(color, colorlistsize_givenlistof4_returns4);
}
